# Set up demo

* Create a new OpenShift namepsace by going to http://cern.ch/web, click in "Create a new website", select:

![](img/Create-new-site.png)


    * Site category: Test site
    * Site type: Paas Web Application


* Fork the gitlab project: https://gitlab.cern.ch/paas-tools/test-devs-cern

![](img/fork.png)

# DEMO 1
## Simple web application deployment

* Go to your OpenShift name space. It shows an empty namespace.

![](img/Add-to-Project.png)

* Let's create an application from the forked repository. It is a Python v3.5 application.

```
Add to project > Browse catalog > Python > 3.5 - latest
```

![](img/python-35.png)

     * Name: Any name
     * Git Repository URL: The Git URL of the forked repository.

* Application will be built automatically by OpenShift. Take a look into the build log, it will clone the reposiory, install the dependecies in requirements.txt, and push the image to the internal Docker registry. When that is ready, take a look to the Deployment procress.



* You can now check your application. You can also login in the pod, and for example see the running processes.

```
Applications > Pods > Name of the pod > Terminal
```

Run for example "ps aux" and "whoami". Please note how there is a single process in the application running with PID 1 - the web application. Also, you can observe the user running the application is not root nor has extra privileges, in fact, it is a given long uid with no entry in /etc/passwd. For this example, these two things don't really matter but it is something you might have to consider for real applications.

 ![](img/Terminal.png)

* Now we can try to scale up the application.

```
Overview > Deployment name
```

 ![](img/Scale-up.png)

* Now we need to add permanent storage so the two pods can share it.

```
Storage > Create Storage
```

 ![](img/Create-storage.png)

     * Storage Class: Standard class
     * Access Mode: Shared Access (RWX)
     * Size: 10 MiB


* Attach volume to our application:

```
Deployment > Deploy name > Actions > Add storage
```

  ![](img/Add-storage.png)

     * Mount Path: /tmp

* A new deployment will be triggered. Now the pods share the storage (the counter is consistent), and will survive reboots.

* Now the app is http only, let's enable SSL.

```
Applications > Routes > Name of the route > Actions > Edit
```

![](img/Route.png)

     * Activate "Secure route"
     * TLS Termination: Edge
     * Insecure Traffic: Redirect

* The certificate is provided automatically by OpenShift.

# DEMO 2

## Protect application with SSO

Let's add SSO protection based on an e-group.

* As the SSO template will try to create a route, we should delete the existing one.

```
Applications > Routes > tesforge (name of the route) > Actions > Delete
```

![](img/Delete-route.png)

* Instantiate the template, setting the egroup to 'it-dep', and the service 'test-app'.

```
Add to Project > Browse Catalog > Uncategorized > cern-sso-proxy (select)
```

![](img/cern-sso-proxy.png)

     * AUTHORIZED_GROUPS: it-dep (or any other)
     * SERVICE_NAME: test-app


* The page is now protected by SSO and shown only to members of the e-group, but it still shows "Hello stranger!". We need to change the code to display the user.

* We will set a web hook so when GitLab makes a change OpenShift applies the change automatically.

```
Builds > builds > test-devs-cern > Configuration > Triggers > Generic Webhook URL (copy)
```

![](img/Triggers.png)

* Go to GitLab:

```
Settings > Integrations
```

![](img/Gitlab-webhook.png)


     * URL: Previously copied URL


* Create merge request

Between "sso_proxy_support" and "master", both branches must in your fork, by default GitLab will select the master branch of the original repository.

* Merge it, the change will be notified to OpenShift, it will start a build and redeploy the application.

* Edit the config map to add more egroups, or change the existing one.

```
Resources > Config Maps > cern-sso-proxy > Actions > Edit
```

Change the configuration from:

```
  <RequireALL>
    Require valid-user
    Require shib-attr ADFS_GROUP it-dep
  </RequireALL>
```

to:

```
  <RequireALL>
    Require valid-user
  </RequireALL>
```

This way any CERN valid user will be able to access the page.

* A change in a config map doesn't trigger a redeploy. To trigger one manually go to:

```
Applications > Deployments > cern-sso-proxy > Deploy
```

* Done
