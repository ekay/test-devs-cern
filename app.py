import os
#import filelock
from flask import Flask, render_template, request
import logging

app = Flask(__name__)
log = logging.getLogger(__name__)
#lock = filelock.FileLock("/tmp/lock")

#def _increment_visitors_number():
#    counter_cache = '/tmp/counter'#
#
#    with lock:
#        try:
#            # Read cache into number
#            with open(counter_cache, mode='r') as cache:
#                number = int(cache.read()) + 1
#        except OSError:
#            # File does not exist, initialize with 1
#            number = 1
#        # Update cache with number of visitors
#        with open(counter_cache, mode='w') as cache:
#            cache.write(str(number))#
#
#    return number

@app.route('/')
def salute():
#    return render_template('hello.html', name=request.headers.get('X-Remote-User'), number=_increment_visitors_number())
    return render_template('hello.html', name=request.headers.get('X-Remote-User'), number=1)
if __name__ == "__main__":
    from waitress import serve
    serve(app, host="0.0.0.0", port=8080)
